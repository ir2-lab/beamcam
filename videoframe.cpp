#include "videoframe.h"

#include "appsink.h"
#include "histogrammer.h"

#include <QPainter>
#include <QLabel>
#include <QGraphicsPixmapItem>

VideoFrame::VideoFrame() :
    pixels(size,0),
    indexed8bit(width,height,QImage::Format_Indexed8), // QImage::Format_Grayscale8),
    pixmap(width,height),
    valid(false),
    m_colormap(cmGray)
{
    updateColormap();
}

void VideoFrame::paintHere(QPainter &p, const QRect &rect) const
{
    if (!pixmap.isNull())
        p.drawPixmap(rect,pixmap);
    else
        p.fillRect(rect,Qt::black);
}

void VideoFrame::paintHere(QLabel* w) const
{
    if (!pixmap.isNull())
        w->setPixmap(pixmap);
}

void VideoFrame::paintHere(QGraphicsPixmapItem* w) const
{
    if (!pixmap.isNull())
        w->setPixmap(pixmap);
}

bool VideoFrame::getNewFrame(AppSink &sink)
{
    if (!(valid = sink.getRawPixels((uchar*)(pixels.data()),size))) return valid;

    // copy pixels to 8 bit img
    // from GRAY16_LE -> 8bit : copy the MSB only
    if (roi_.isNull()) {
        uchar* i = indexed8bit.bits();
        const uchar* p = (const uchar*)(pixels.constData())+1;
        const uchar* pend = p+size;
        while(p<pend) { *i++=*p++; p++; }
    } else {
        const uchar* p = (const uchar*)(pixels.constData()) + 1 + 2*roi_.left() + 2*width*roi_.top();
        for(int k=0; k<roi_.height(); k++) {
            uchar* i = indexed8bit.scanLine(k);
            uchar* iend = i+roi_.width();
            const uchar* q = p;
            while(i!=iend) { *i++=*q++; q++; }
            p += 2*width;
        }
    }

    return valid = pixmap.convertFromImage(indexed8bit);

}

void VideoFrame::setColormap(Colormap cm)
{
    m_colormap = cm;
    updateColormap();
}

void VideoFrame::cursorData(QVector<QPointF> &v, int idx, bool hrz) const
{
    // check that idx points inside roi
    int imin,imax;
    if (hrz)
    {
        imin=0; imax=height-1;
        if (!roi_.isNull()) { imin = roi_.top(); imax = roi_.bottom()-1; }
    } else {
        imin=0; imax=width-1;
        if (!roi_.isNull()) { imin = roi_.left(); imax = roi_.right()-1; }
    }
    if (idx<imin) idx = imin;
    else if (idx>imax) idx=imax;

    const QVector<int>& curve16 = transferFunction.getCurve16();

    // how many pixels to copy
    int npix;
    if (hrz) npix = roi_.isNull() ? width : roi_.width();
    else npix = roi_.isNull() ? height : roi_.height();
    v.resize(npix);

    const quint16 *p = reinterpret_cast<const quint16 *>(pixels.constData());
    if (hrz) {
        p += idx*width;
        if (!roi_.isNull()) p += roi_.left();
        for(int i=0; i<npix; i++)
        {
            v[i].setX(1.*i/npix);
            v[i].setY(curve16[*p++]);
        }
    }
    else {
        p += idx;
        int stride = width;
        if (!roi_.isNull()) p += stride*roi_.top();
        for(int i=0; i<npix; i++)
        {
            v[i].setX(1.*i/npix);
            v[i].setY(curve16[*p]);
            p += stride;
        }
    }
}

void VideoFrame::updateColormap()
{
    int ci = (int)m_colormap;
    QVector<QRgb> cmdata(256);
    QRgb* data = cmdata.data();
    const QVector<int>& curve = transferFunction.getCurve();
    for(int i=0; i<256; i++)
        data[i] = ColormapData[ci][curve[i]];

    indexed8bit.setColorTable(cmdata);
}

void VideoFrame::setRoi(const QRect& r)
{
    if (r==roi_) return;
    if (r.isNull()) {
        indexed8bit = QImage(width,height,QImage::Format_Indexed8);
        pixmap = QPixmap(width,height);
    } else {
        QRect r1 = r.normalized();
        if (r1.left()<0 || r1.top()<0 || r1.right()>width || r1.bottom()>height) return;
        indexed8bit = QImage(r.width(),r.height(),QImage::Format_Indexed8);
        pixmap = QPixmap(r.width(),r.height());
    }
    roi_ = r;
}
