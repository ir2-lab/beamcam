#ifndef CAMERA_H
#define CAMERA_H

#include "appsink.h"
#include "videoframe.h"
#include "counter.h"
#include "histogrammer.h"

#include <QGst/Init>
#include <QGst/Pipeline>
#include <QGst/ElementFactory>
#include <QGst/Message>


#include <QObject>
#include <QString>
#include <QList>
#include <QSize>
//#include <QAtomicInt>
#include <QTimer>
//#include <QTime>
#include <QImage>

class Camera : public QObject
{
    Q_OBJECT
public:
    const static int shutterSpeedStopsN = 11;
    static int shutterSpeedStops[shutterSpeedStopsN];

    struct CameraInfo
    {
        QString name, identifier, connection_type, serial;
        bool isTest;
        CameraInfo() {}
        CameraInfo(const char* a, const char* b, const char* c, const char* d) :
            name(a), identifier(b), connection_type(c), serial(d)
        { isTest = serial == "0";}
    };

    static QList<CameraInfo> enumDevices();
    static QSize frameSize() { return QSize(VideoFrame::width,VideoFrame::height); }
    static QSize minimumViewSize() { return QSize(320,240); }

    static QString capsString();

    explicit Camera(QObject *parent = 0);
    ~Camera();

    bool open(const CameraInfo& cam);

    const CameraInfo& cameraInfo() const { return m_info; }

    bool isRunning() const;
    bool isOK() const { return m_pipeline; }

    int gain() const { return m_gain; }
    int exposure() const { return m_exposure; }

    void setGain(int g);
    void setExposure(int e);

    void setColormap(VideoFrame::Colormap cm)
    { m_videoFrame.setColormap(cm); }
    void setBrightness(int b)
    { m_videoFrame.setBrightness(b); }
    void setContrast(int c)
    { m_videoFrame.setContrast(c); }
    void setGamma(qreal g)
    { m_videoFrame.setGamma(g); }


    QString statusMsg() const;

    const VideoFrame& videoFrame() const { return m_videoFrame; }
    const QVector<QPointF>& histogram() const { return m_histogram; }

    void setRoi(const QRect& r) { m_videoFrame.setRoi(r); }


signals:
    void stateChanged();
    void propertiesChanged();
    void newVideoFrame();
    void newHistogram();

public slots:
    void start();
    void stop();
    void setHistoOn(bool on);

private slots:
    void onTimer();
    void onStateChange(const QGst::State& oldst, const QGst::State &newst);

    void onFrame();

private:

    QGst::PipelinePtr m_pipeline;
    QGst::ElementPtr m_src;
    AppSink m_appsink;

    CameraInfo m_info;
    int m_gain, m_exposure;

    QTimer m_timer;

    VideoFrame m_videoFrame;

    bool m_histoOn;
    QVector<QPointF> m_histogram;
    Histogrammer histogrammer;

    counter frameCounter;

    bool createPipeline();
    void onBusMessage(const QGst::MessagePtr & message);
    void getProperties();

    bool getProperty(const char* name, int& v);
    bool setProperty(const char* name, int v);
};





#endif // CAMERA_H
