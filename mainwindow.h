#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>


namespace Ui {
class MainWindow;
}

class Camera;
class QLabel;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void prepareDeviceMenu();
    void setDevice();

    void on_actionZoomOne2One_triggered();

    void on_sbGain_valueChanged(int g);
    void on_sldExposure_valueChanged(int p);

    void onCameraPropertiesChanged();
    void onCameraStatusChanged();

    void on_comboBox_currentIndexChanged(int index);



    void on_sbBrightness_valueChanged(int b);

    void on_sbContrast_valueChanged(int arg1);

    void on_sbGamma_valueChanged(double arg1);

    void on_sldGamma_valueChanged(int value);

    void on_actionROI_triggered(bool checked);

    void on_actionZoom_to_ROI_triggered(bool checked);

private:
    Ui::MainWindow *ui;

    Camera* m_cam;
    QLabel* m_camStatus;

    void updateCameraProperties();

};

#endif // MAINWINDOW_H
