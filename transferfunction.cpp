#include "transferfunction.h"

TransferFunction::TransferFunction() :
    m_brightness(0),
    m_contrast(0),
    m_gamma(1.),
    curve(1 << 8), curve16(1 << 16)
{
    updateCurve();
}

void TransferFunction::updateCurve()
{
    qreal b = 1.*m_brightness/128;
    qreal c = 1.*m_contrast/128;
    qreal x1 = (-0.5-b)/(1.+c);
    if (x1<-0.5) x1 = -0.5;
    qreal x2 = (0.5-b)/(1.+c);
    if (x2>0.5) x2 = 0.5;
    qreal y1 = (1+c)*x1+b;
    qreal y2 = (1+c)*x2+b;

    for(int i=0; i<256; i++)
    {
        qreal x = 1.*i/255 - 0.5, y;

        if (x<=x1) y = y1;
        else if (x>=x2) y = y2;
        else {
            x = (x-x1)/(x2-x1);
            y = pow(x,m_gamma);
            y = y*(y2-y1) + y1;
        }

        curve[i]=floor((y+0.5)*255);
    }

    for(int i=0; i<65536; i++)
    {
        qreal x = 1.*i/65535 - 0.5, y;
        if (x<=x1) y = y1;
        else if (x>=x2) y = y2;
        else {
            x = (x-x1)/(x2-x1);
            y = pow(x,m_gamma);
            y = y*(y2-y1) + y1;
        }

        curve16[i]=floor((y+0.5)*65535);
    }

}
