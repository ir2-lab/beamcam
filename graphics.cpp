#include "graphics.h"
#include "camera.h"

#include <QResizeEvent>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QGraphicsLineItem>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>

#include <QCursor>
#include <QChart>
#include <QLineSeries>
#include <QAreaSeries>
#include <QValueAxis>
#include <QLogValueAxis>

enum Direction {
    Vertical = 0,
    Horizontal = 1
};

#define LINEW 3

static QColor hColor = Qt::cyan; //Qt::blue;
static QColor vColor = Qt::magenta; //darkGreen;

class LineCursor : public QGraphicsLineItem
{
public:

    Direction dir;

    explicit LineCursor(Direction d = Vertical) : dir(d)
    {
        if (dir==Vertical)
        {
            setLine(VideoFrame::width/2,0,VideoFrame::width/2,VideoFrame::height);
            setPen(QPen(QBrush(vColor),LINEW));
        }
        else
        {
            setLine(0,VideoFrame::height/2,VideoFrame::width,VideoFrame::height/2);
            setPen(QPen(QBrush(hColor),LINEW));
        }
        setFlag(QGraphicsItem::ItemIsMovable, true);
        setFlag(QGraphicsItem::ItemIsSelectable, true);
        setFlag(QGraphicsItem::ItemIsFocusable, true);
        setAcceptHoverEvents(true);
    }

    int index() const
    {
        QPointF p = pos();
        return dir ? int(p.y() + 0.5*VideoFrame::height) : int(p.x() + 0.5*VideoFrame::width);
    }

    void setLineWidth(int w)
    {
        if (dir) setPen(QPen(QBrush(hColor),w));
        else setPen(QPen(QBrush(vColor),w));
    }

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsLineItem::mouseMoveEvent(event);

        if (isSelected())
        {
            QPointF p = pos();
            if (dir==Vertical) {
                p.setY(0);
                int x = (int)p.x();
                if (x >= VideoFrame::width/2) p.setX(VideoFrame::width/2-1.);
                else if (x < -VideoFrame::width/2) p.setX(-VideoFrame::width/2);
            } else {
                p.setX(0);
                int y = (int)p.y();
                if (y >= VideoFrame::height/2) p.setY(VideoFrame::height/2-1.);
                else if (y < -VideoFrame::height/2) p.setY(-VideoFrame::height/2);
            }
            setPos(p);
        }
    }
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event)
    {
        //setCursor(Qt::SizeVerCursor);
        QGraphicsLineItem::hoverEnterEvent(event);
    }
    void hoverMoveEvent(QGraphicsSceneHoverEvent* event)
    {
        setCursor(dir ? Qt::SizeVerCursor : Qt::SizeHorCursor);
        QGraphicsLineItem::hoverMoveEvent(event);
    }
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
    {
        if(mouseEvent->modifiers() & Qt::ControlModifier)
        {
            mouseEvent->ignore();
        }
        else QGraphicsLineItem::mousePressEvent(mouseEvent);
    }

};

class RoiRect : public QGraphicsRectItem
{
    const static int linew = 6;
public:
    RoiRect() : m_isResizing(false)
    {
        setFlag(QGraphicsItem::ItemIsMovable, true);
        setFlag(QGraphicsItem::ItemIsSelectable, true);
        setFlag(QGraphicsItem::ItemIsFocusable, true);

        setAcceptHoverEvents(true);
        setRect(0,0,400.,300.);
        setPos(VideoFrame::width/2 - 200,VideoFrame::height/2 - 150);
        setPen(QPen(QBrush(Qt::white),6,Qt::DashLine));
    }
    void setLineWidth(int w)
    {
        setPen(QPen(QBrush(Qt::white),w,Qt::DashLine));
    }
protected:
    bool m_isResizing;
    bool isInResizeArea(const QPointF &pos)
    {
        QRectF rect = boundingRect();
        return (rect.width() - pos.x()<20 && rect.height() - pos.y()<20);
    }
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event)
    {
        if (m_isResizing) {
            qreal x = event->pos().x();
            qreal xmax = VideoFrame::width - pos().x();
            prepareGeometryChange();
            if (x<120) x=120;
            else if (x>xmax) x=xmax;
            setRect(0,0,x,3.*x/4);
        } else {
            QGraphicsRectItem::mouseMoveEvent(event);
            if (isSelected())
            {
                QPointF p = pos(), newpos(p);
                QRectF rect = boundingRect();
                qreal xmax = VideoFrame::width - rect.width();
                qreal ymax = VideoFrame::height - rect.height();
                if (p.x()<0) newpos.setX(0);
                if (p.y()<0) newpos.setY(0);
                if (p.x()>xmax) newpos.setX(xmax);
                if (p.y()>ymax) newpos.setY(ymax);
                if (p!=newpos) setPos(newpos);
            }
        };
    }
    void mousePressEvent(QGraphicsSceneMouseEvent *event)
    {
        static qreal z = 0.0;
        setZValue(z += 1.0);
        if (event->button() == Qt::LeftButton && isInResizeArea(event->pos())) {
            m_isResizing = true;
        } else {
            QGraphicsRectItem::mousePressEvent(event);
        }
    }

    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
    {
        if (event->button() == Qt::LeftButton && m_isResizing) {
            m_isResizing = false;
        } else {
            QGraphicsRectItem::mouseReleaseEvent(event);
        }
    }
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event)
    {
        //setCursor(Qt::SizeVerCursor);
        QGraphicsRectItem::hoverEnterEvent(event);
    }
    void hoverMoveEvent(QGraphicsSceneHoverEvent* event)
    {
        //setCursor(Qt::SizeAllCursor); && isSelected()
        if (m_isResizing || (isInResizeArea(event->pos())))
            setCursor(Qt::SizeFDiagCursor);
        else
            setCursor(Qt::SizeAllCursor);
        QGraphicsRectItem::hoverMoveEvent(event);
    }
};

class CursorChart : public QChart
{
public:
    Direction dir;
    QLineSeries *series;
    QVector<QPointF> data;
    CursorChart(Direction d = Vertical) : dir(d),
        data(d==Horizontal ? VideoFrame::width : VideoFrame::height)
    {
        setTheme(QChart::ChartThemeDark);
        legend()->hide();

        series = new QLineSeries;
        series->append(0.,-100.);
        series->append(1.,66000.);
        if (dir==Horizontal) {           
            QPen p(hColor);
            p.setWidth(2);
            series->setPen(p);
        }
        else {
            QPen p(vColor);
            p.setWidth(2);
            series->setPen(p);
        }
        //series->setUseOpenGL(true);
        addSeries(series);

        createDefaultAxes();
        axisX()->setLabelsFont(QFont("Sans Serif",1));
        axisY()->setLabelsFont(QFont("Sans Serif",1));
        axisX()->setLabelsVisible(false);
        axisY()->setLabelsVisible(false);

        setMargins(QMargins(0,5,5,0));
    }
    void redraw()
    {
        series->replace(data);
    }
    void setLineWidth(int w)
    {
        if (dir)
            series->setPen(QPen(QBrush(hColor),w));
        else
            series->setPen(QPen(QBrush(vColor),w));
    }
};

class Histogram : public QChart
{
public:
    QAreaSeries *series;
    //QLineSeries *series;

    Histogram()
    {
        setTheme(QtCharts::QChart::ChartThemeDark);
        legend()->hide();

        QValueAxis *axisX = new QValueAxis;
        axisX->setTickCount(6);
        axisX->setLabelFormat("%i");
        axisX->setLabelsVisible(false);
        addAxis(axisX, Qt::AlignBottom);

        QLogValueAxis *axisY = new QLogValueAxis; //QLogValueAxis;
        axisY->setLabelFormat("%5.0e");
        axisY->setLabelsVisible(false);
        addAxis(axisY, Qt::AlignLeft);

        QLineSeries *lineseries= new QLineSeries;
        lineseries->append(-1.,0.0001);
        lineseries->append(256.,1.1);

        series = new QAreaSeries(lineseries);
        series->setUseOpenGL(true);
        series->setColor(Qt::gray);
        series->setBorderColor(Qt::gray);
        series->attachAxis(axisX);
        series->attachAxis(axisY);
        addSeries(series);
    }
};

class PixmapItem : public QGraphicsPixmapItem
{
public:
    PixmapItem()
    {
        //setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
    }
    virtual QRectF	boundingRect()
    { return QRectF(0.,0.,VideoFrame::width,VideoFrame::height); }
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = Q_NULLPTR)
    {
        Q_UNUSED(option);
        Q_UNUSED(widget);
        painter->drawPixmap(boundingRect().toRect(), pixmap());
    }

protected:
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
    {
        QPointF p = event->pos();
        GraphicsScene* sc = (GraphicsScene*)scene();
        sc->hCursor->setPos(0.,p.y()-VideoFrame::height/2);
        sc->vCursor->setPos(p.x()-VideoFrame::width/2,0.);
        QGraphicsPixmapItem::mouseDoubleClickEvent(event);
    }
};

GraphicsScene::GraphicsScene() :
    m_cursorsOn(false),
    m_histoOn(false), m_roiOn(false)
{
    qreal u = 1.*VideoFrame::width/4;

    videoItem = new PixmapItem;

    addItem(videoItem);

    hCursor = new LineCursor(Horizontal);
    vCursor = new LineCursor(Vertical);

    roiRect = new RoiRect;

    hChart = new CursorChart(Horizontal);
    hChart->setGeometry(0.,3*u,4*u,2*u);

    vChart = new CursorChart(Vertical);
    vChart->setGeometry(0.,0.,3*u,2*u);
    vChart->setPos((5 - 3./2)*u,(1.5 - 1.)*u);
    vChart->setTransformOriginPoint(3.*u/2,1.*u);
    vChart->setRotation(90.);

    hist = new Histogram;
    hist->setGeometry(4*u,3*u,2*u,2*u);

    layoutItems();
}

void GraphicsScene::setCursorsOn(bool on)
{
    if (on != m_cursorsOn)
    {
        m_cursorsOn = on;
        layoutItems();
    }
}

void GraphicsScene::setHistoOn(bool on)
{
    if (on != m_histoOn)
    {
        m_histoOn = on;
        layoutItems();
    }
}

void GraphicsScene::setRoiOn(bool on)
{
    if (on != m_roiOn)
    {
        m_roiOn = on;
        if (on) m_zoom2Roi = false;
        layoutItems();
    }
}

void GraphicsScene::setZoom2Roi(bool on)
{
    if (on != m_zoom2Roi)
    {
        m_zoom2Roi = on;
        if (on) {
            m_roiOn = false;
        }
        layoutItems();
    }
}

void GraphicsScene::layoutItems()
{
    qreal u = 1.*VideoFrame::width/4;

    removeItem(videoItem);
    hCursor->setParentItem(0);
    vCursor->setParentItem(0);
    roiRect->setParentItem(0);
    removeItem(hChart);
    removeItem(vChart);
    removeItem(roiRect);
    removeItem(hist);

    hCursor->setSelected(false);
    vCursor->setSelected(false);
    roiRect->setSelected(false);

    if (m_cursorsOn)
    {
        setSceneRect(0.,0.,6*u,5*u);

        videoItem->setPos(23,15);
        videoItem->setScale(1240./1280);

        hCursor->setParentItem(videoItem);
        vCursor->setParentItem(videoItem);

        addItem(videoItem);
        addItem(hChart);
        addItem(vChart);
        if (m_histoOn) {
            hist->setGeometry(4*u,3*u,2*u,2*u);
            addItem(hist);
        }
    }
    else
    {
        if (m_histoOn)
        {
            setSceneRect(0.,0.,5*u,3*u);
            videoItem->setPos(0.,0.);
            videoItem->setScale(1.);
            addItem(videoItem);
            hist->setGeometry(4*u,0.,u,u);
            addItem(hist);
        }
        else
        {
            setSceneRect(0.,0.,4*u,3*u);
            videoItem->setPos(0.,0.);
            videoItem->setScale(1.);
            addItem(videoItem);
        }
    }

    if (m_roiOn) {
        roiRect->setParentItem(videoItem);
        addItem(roiRect);
    }
}

qreal GraphicsScene::scalingFactor(const QSize &sz) const
{
    qreal w = sz.width(), h = sz.height();
    if (m_cursorsOn) {
        w = 2*w/3; h = 3*h/5;
    }
    else if (m_histoOn)
    {
        w = 4*w/5;
    }
    qreal q = 3*w-4*h;

   return q>=0. ? 4*h/3/VideoFrame::width :
                  3*w/4/VideoFrame::height;
}

QRect GraphicsScene::roi() const
{
    return videoItem->mapRectFromItem(roiRect, roiRect->boundingRect()).toRect();
}

QPoint GraphicsScene::crossHair() const
{
    QPointF p;
    if (m_zoom2Roi) {
        QRectF r = videoItem->mapRectFromItem(roiRect, roiRect->boundingRect());
        p.setX(r.left() + r.width()*vCursor->index()/VideoFrame::width);
        p.setY(r.top() + r.height()*hCursor->index()/VideoFrame::height);
    } else {
        p.setX(vCursor->index());
        p.setY(hCursor->index());
    }
    return p.toPoint();
}

void GraphicsScene::adjustLineWidths(qreal f)
{
    int w = int(1.*LINEW/f);
    hCursor->setLineWidth(w);
    vCursor->setLineWidth(w);
    roiRect->setLineWidth(w);
    hChart->setLineWidth(int(2./f));
    vChart->setLineWidth(int(2./f));
}

GraphicsView::GraphicsView(QWidget *parent) : QGraphicsView(parent),
    m_camera(0)
{
    //setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    //setRenderHints(QPainter::SmoothPixmapTransform);
    setBackgroundBrush(Qt::gray);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    m_scene = new GraphicsScene;
    setScene(m_scene);
}

void GraphicsView::setCamera(Camera * c)
{
    if (m_camera) {
        disconnect(m_camera,SIGNAL(newVideoFrame()),this,SLOT(onNewFrame()));
    }
    m_camera = c;
    if (m_camera) {
        connect(m_camera,SIGNAL(newVideoFrame()),this,SLOT(onNewFrame()));
    }
}

void GraphicsView::resizeEvent(QResizeEvent *event)
{
    qreal f = m_scene->scalingFactor(event->size());
    m_scene->adjustLineWidths(f);

    resetTransform();
    scale(f,f);

    QGraphicsView::resizeEvent(event);
}

void GraphicsView::onNewFrame()
{
    const VideoFrame& frm = m_camera->videoFrame();
    frm.paintHere(m_scene->videoItem);

    if (m_scene->cursorsOn())
    {
        QPoint p = m_scene->crossHair();
        frm.cursorData(m_scene->hChart->data,p.y(),1);
        frm.cursorData(m_scene->vChart->data,p.x(),0);
        m_scene->hChart->redraw();
        m_scene->vChart->redraw();
    }
    if (m_scene->histoOn())
    {
        const QVector<QPointF>& h = m_camera->histogram();
        m_scene->hist->series->upperSeries()->replace(h);
    }
}

void GraphicsView::onCursorsOn(bool on)
{
    m_scene->setCursorsOn(on);
    qreal f = m_scene->scalingFactor(size());
    resetTransform();
    scale(f,f);
}

void GraphicsView::onHistoOn(bool on)
{
    m_camera->setHistoOn(on);
    m_scene->setHistoOn(on);
    qreal f = m_scene->scalingFactor(size());
    resetTransform();
    scale(f,f);
}

void GraphicsView::onRoiOn(bool on)
{
    m_scene->setRoiOn(on);
}

void GraphicsView::onZoom2Roi(bool on)
{
    m_scene->setZoom2Roi(on);
}

qreal GraphicsView::getScalingFactor() const
{
    qreal f = m_scene->scalingFactor(size());
    if (m_scene->isZoom2Roi())
    {
        f *= 1.*VideoFrame::width/m_scene->roi().width();
    }
    return f;
}
