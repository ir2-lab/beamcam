#include "camera.h"

#include <tcamprop.h>

#include <QDebug>
#include <QMessageBox>
#include <QCoreApplication>

#include <QGlib/Error>
#include <QGlib/Connect>
#include <QGst/Bus>
#include <QGst/Pad>
#include <QGst/Caps>
#include <QGst/Structure>

int Camera::shutterSpeedStops[Camera::shutterSpeedStopsN] = {1000, 500, 250, 125, 60, 30, 15, 8, 4, 2, 1};

QString Camera::capsString()
{
    return QString("video/x-raw, format=GRAY16_LE, width=1280, height=960, framerate=15/1");
}

QList<Camera::CameraInfo> Camera::enumDevices()
{
    QList<CameraInfo> lst;

    // Test video
    lst << CameraInfo("Test Video Source - Pattern", "GStreamer Device", "v4l2", "0");
    lst << CameraInfo("Test Video Source - Snow", "GStreamer Device", "v4l2", "0");

    /* create a tcambin to retrieve device information */
    QGst::ElementPtr source = QGst::ElementFactory::make("tcambin", "source");

    /* retrieve a single linked list of serials of the available devices */
    GSList* serials = tcam_prop_get_device_serials(TCAM_PROP(source));

    for (GSList* elem = serials; elem; elem = elem->next)
    {
        char* name;
        char* identifier;
        char* connection_type;

        /* This fills the parameters to the likes of:
           name='DFK Z12GP031',
           identifier='The Imaging Source Europe GmbH-11410533'
           connection_type='aravis'
           The identifier is the name given by the backend
           The connection_type identifies the backend that is used.
                   Currently 'aravis', 'v4l2' and 'unknown' exist
        */
        gboolean ret = tcam_prop_get_device_info(TCAM_PROP(source),
                                                 (gchar*) elem->data,
                                                 &name,
                                                 &identifier,
                                                 &connection_type);

        if (ret) // get_device_info was successfull
        {
            lst << CameraInfo(name,identifier,connection_type,(gchar*) elem->data);
        }
    }

    return lst;
}

Camera::Camera(QObject *parent) : QObject(parent),
    m_histoOn(false), m_histogram(1 << 8)
{

    for(int i=0; i<m_histogram.size(); i++)
        m_histogram[i].setX(1.*i);

    connect(&m_timer,SIGNAL(timeout()),this,SLOT(onTimer()));
    connect(&m_appsink,SIGNAL(sampleAvailable()),this,SLOT(onFrame()));
}

Camera::~Camera()
{
    stop();
}

bool Camera::open(const CameraInfo &cam)
{
    stop();
    if (m_pipeline) m_pipeline.clear();
    if (m_src) m_src.clear();
    m_appsink.setElement(QGst::ElementPtr());
    m_info = cam;
    return createPipeline();
}

bool Camera::createPipeline()
{
    QString pipeline_description(m_info.isTest ? "videotestsrc" : "tcamsrc");
    pipeline_description += " name=src";
    if (m_info.isTest) {
        if (m_info.name==QString("Test Video Source - Snow")) pipeline_description += " pattern=snow";
    } else pipeline_description += QString(" serial=%1").arg(m_info.serial);
    pipeline_description += " ! ";
    pipeline_description += capsString();
    pipeline_description += " ! appsink name=app";

    QGst::BinPtr b =  QGst::Bin::fromDescription(pipeline_description);
    if (!b) return false;
    m_pipeline = QGst::Pipeline::create();
    m_pipeline->add(b);
    m_src = m_pipeline->getElementByName("src");
    QGst::ElementPtr appsink = m_pipeline->getElementByName("app");
    m_appsink.setElement(appsink);

    start();

    return m_pipeline;
}

bool Camera::isRunning() const
{
    if (!m_pipeline) return false;
    QGst::State currentState;
    m_pipeline->getState(&currentState, NULL, 0);
    return currentState == QGst::StatePlaying;

}

void Camera::start()
{
    if (m_pipeline) {
        //connect the bus

        m_pipeline->bus()->addSignalWatch();

        QGlib::connect(m_pipeline->bus(), "message", this, &Camera::onBusMessage);

        m_pipeline->setState(QGst::StatePlaying);

        getProperties();
        frameCounter.reset();
        m_timer.start(1000);
        m_appsink.reset();

    }

}

void Camera::onStateChange(const QGst::State &oldst,const QGst::State &newst)
{
//    if (newst == QGst::StatePlaying)
//    {
//        if (oldst == QGst::StateNull)
//        {
//            m_gain = 0; m_exposure = 0;
//            setGain(174);
//            setExposure(60);
//        }
//    }

    emit stateChanged();
}

void Camera::onBusMessage(const QGst::MessagePtr & message)
{
    switch (message->type()) {
    case QGst::MessageEos:
        //got end-of-stream - stop the pipeline
        stop();
        break;
    case QGst::MessageError:
        //check if the pipeline exists before destroying it,
        //as we might get multiple error messages
        if (m_pipeline) {
            stop();
        }
        QMessageBox::critical(qobject_cast<QWidget*>(parent()), tr("Pipeline Error"),
                              message.staticCast<QGst::ErrorMessage>()->error().message());
        break;
    case QGst::MessageStateChanged:
        onStateChange(message.staticCast<QGst::StateChangedMessage>()->oldState(),
                      message.staticCast<QGst::StateChangedMessage>()->newState());
        emit stateChanged();
        break;

    default:
        break;
    }
}

void Camera::onTimer()
{
    emit stateChanged();
}

void Camera::stop()
{
    if (m_pipeline) {
        m_pipeline->setState(QGst::StateNull);
    }
    m_timer.stop();
}

void Camera::getProperties()
{
    if (!isOK() || m_info.isTest)
    {
        m_gain = 174;
        m_exposure = 1000;
    }
    else
    {
        getProperty("Gain",m_gain);
        getProperty("Exposure",m_exposure);
    }
}



void Camera::setGain(int g)
{
    if (g == m_gain) return;
    if (!m_src) return;
    if (!m_info.isTest) setProperty("Gain",g);
    m_gain = g;
    emit propertiesChanged();
}

void Camera::setExposure(int e)
{
    if ( e == m_exposure ) return;
    if (!m_src) return;
    if (!m_info.isTest) setProperty("Exposure",e);
    m_exposure = e;
    emit propertiesChanged();
}

QString Camera::statusMsg() const
{
    QString msg;
    if (m_pipeline)
    {
        msg += isRunning() ? "FPS: " : "CAMERA OFF";
        if (isRunning()) {
            msg += QString(" CAM=%1").arg(m_appsink.fps(),3,'f',1);
            msg += QString(", DISP=%1").arg(frameCounter.fps(),3,'f',1);
        }
    } else msg += "ERROR";
    return msg;
}

void Camera::onFrame()
{
    if (m_videoFrame.getNewFrame(m_appsink))
    {
        if (m_histoOn) {
            histogrammer.proccess(m_videoFrame);
            const QVector<qreal>& h = histogrammer.getHistogram();
            // copy data
            for(int i=0; i<m_histogram.size(); i++)
                m_histogram[i].setY(h[i]);
        }
        frameCounter.tic();
        emit newVideoFrame();
    }
}

bool Camera::getProperty(const char* name, int& v)
{
    if (!m_src || m_info.isTest) return false;
    QGlib::Value val,min,max,def,step,type,flags,cat,gr;
    bool ret = tcam_prop_get_tcam_property(TCAM_PROP(m_src),(gchar*)name,val,min,max,def,step,type,flags,cat,gr);
    if (ret) v = val.toInt();
    return ret;
}

bool Camera::setProperty(const char* name, int v)
{
    if (!m_src || m_info.isTest) return false;
    QGlib::Value val(v);
    return tcam_prop_set_tcam_property(TCAM_PROP(m_src),(gchar*)name,val);
}

void Camera::setHistoOn(bool on)
{
    if (on==m_histoOn) return;

    m_histoOn = on;
}


