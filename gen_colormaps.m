clear

N = 256;

function print_cmap(fid,map)
  cmap = map(:,3) + 256*(map(:,2) + 256*(map(:,1) + 256*255));

  fprintf(fid,"  {\n");  
  k = 1;
  for i=1:63,
    fprintf(fid,"    ");
    for j=1:4,
      fprintf(fid,"0x%x, ",cmap(k));
      k = k+1;
    endfor
    fprintf(fid,"\n");
  endfor
  fprintf(fid,"    ");
  for j=1:3,
    fprintf(fid,"0x%x, ",cmap(k));
    k = k+1;
  endfor
  fprintf(fid,"0x%x",cmap(k));
  fprintf(fid,"\n");
  fprintf(fid,"  }");
endfunction  
  

fid = fopen('colormaps.cpp','wt');

fprintf(fid,'#include "videoframe.h"\n\n');
fprintf(fid,'QRgb VideoFrame::ColormapData[4][256] = {\n');
print_cmap(fid,ceil(gray(256)*255));
fprintf(fid,",\n");
print_cmap(fid,ceil(jet(256)*255));
fprintf(fid,",\n");
print_cmap(fid,ceil(hot(256)*255));
fprintf(fid,",\n");
print_cmap(fid,ceil(copper(256)*255));
fprintf(fid,"\n");
fprintf(fid,"};\n\n");

fclose(fid);

