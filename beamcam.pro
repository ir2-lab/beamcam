#-------------------------------------------------
#
# Project created by QtCreator 2017-09-07T15:30:43
#
#-------------------------------------------------

QT       += core gui charts widgets opengl

CONFIG += link_pkgconfig

PKGCONFIG += tcam gobject-introspection-1.0

INCLUDEPATH += /usr/include/qt5

PKGCONFIG += Qt5GStreamer-1.0
# PKGCONFIG += Qt5GStreamerUi-1.0
PKGCONFIG += Qt5GStreamerUtils-1.0;

TARGET = beamcam
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    camera.cpp \
    appsink.cpp \
    videoframe.cpp \
    colormaps.cpp \
    graphics.cpp \
    histogrammer.cpp \
    transferfunction.cpp

HEADERS  += mainwindow.h \
    camera.h \
    appsink.h \
    videoframe.h \
    graphics.h \
    histogrammer.h \
    counter.h \
    transferfunction.h

FORMS    += mainwindow.ui

DISTFILES += \
    gen_colormaps.m \
    beamcam.desktop \
    beamcam.spec \
    icons/selection-128.png

target.path = /usr/bin
desktop.path = /usr/share/applications
desktop.files += beamcam.desktop
icons.path = /usr/share/icons/hicolor/48x48/apps
icons.files += icons/48x48/beamcam.png
INSTALLS += target desktop icons

RESOURCES += \
    beamcam.qrc
