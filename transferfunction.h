#ifndef TRANSFERFUNCTION_H
#define TRANSFERFUNCTION_H

#include <QVector>

class TransferFunction
{
public:
    TransferFunction();

    int brightness() const { return m_brightness; }
    int contrast() const { return m_contrast; }
    int gamma() const { return m_gamma; }

    void setBrightness(int b)
    { m_brightness = b; updateCurve(); }
    void setContrast(int c)
    { m_contrast = c; updateCurve(); }
    void setGamma(qreal g)
    { m_gamma = g; updateCurve(); }

    const QVector<int>& getCurve() const
    { return curve; }
    const QVector<int>& getCurve16() const
    { return curve16; }

private:
    int m_brightness, m_contrast;
    qreal m_gamma;

    QVector<int> curve,curve16;

    void updateCurve();
};

#endif // TRANSFERFUNCTION_H
