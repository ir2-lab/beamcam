Summary: Qt-based Beam Camera Viewer.
Name: beamcam
Version: 0.3
Release: 1%{?dist}
License: MIT
Source0: %{name}-%{version}.tar.gz

Requires: qt5-qtbase
Requires: qt5-qtbase-gui
Requires: qt5-qtcharts
Requires: qt5-gstreamer
Requires: gstreamer1 gstreamer1-plugins-base
Requires: tiscamera

BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtcharts-devel
BuildRequires: qt5-gstreamer-devel
BuildRequires: tiscamera
BuildRequires: gobject-introspection-devel

%description
A program to view and process the beam camera video stream.

%prep
%setup -q

%build
mkdir %{_target_platform}
pushd %{_target_platform}
%{qmake_qt5} ..
make %{?_smp_mflags}
popd

%install
rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%defattr(-,root,root)
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/48x48/apps/%{name}.png

%changelog
* Thu Oct 5 2017 George
- ver 0.3

* Tue Sep 26 2017 George
- ver 0.2
- initial version of spec file
