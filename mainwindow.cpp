#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "camera.h"

#include <QMessageBox>
#include <QTimer>
#include <QLabel>

#include <QGLWidget>

#include "graphics.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    m_cam = new Camera(this);

    ui->setupUi(this);

    QIcon appicon(":/beamcam.png");
    setWindowIcon(appicon);

    ui->sldExposure->setRange(0,Camera::shutterSpeedStopsN-1);
    ui->sldExposure->setPageStep(1);
    ui->sldExposure->setValue(0);

    GraphicsView* view = new GraphicsView;
    view->setMinimumSize(Camera::minimumViewSize());
    view->setCamera(m_cam);
    view->setViewport(new QGLWidget());

    setCentralWidget(view);

    //adjustSize(); // resize to fit the view camera

    setWindowTitle("Beam Camera");

    connect(ui->menuDevice,SIGNAL(aboutToShow()),this,SLOT(prepareDeviceMenu()));

    m_camStatus = new QLabel(this);
    ui->statusBar->addPermanentWidget(m_camStatus);

    /* find the cameras */
    QList<Camera::CameraInfo> devs = Camera::enumDevices();
    if (devs.isEmpty())
    {
        QMessageBox::critical(this,"beamcam","no cameras found.");
        QTimer::singleShot(0, this, SLOT(close()));
    }

    connect(m_cam,SIGNAL(propertiesChanged()),this,SLOT(onCameraPropertiesChanged()));
    connect(m_cam,SIGNAL(stateChanged()),this,SLOT(onCameraStatusChanged()));

    connect(ui->actionLineCursors,SIGNAL(toggled(bool)),view,SLOT(onCursorsOn(bool)));
    connect(ui->actionHistogram,SIGNAL(toggled(bool)),view,SLOT(onHistoOn(bool)));
    //connect(ui->actionROI,SIGNAL(toggled(bool)),view,SLOT(onRoiOn(bool)));
    //connect(ui->actionZoom_to_ROI,SIGNAL(toggled(bool)),view,SLOT(onZoom2Roi(bool)));


    m_cam->open(devs.last());
    updateCameraProperties();

}

MainWindow::~MainWindow()
{

    m_cam->stop();

    delete ui;
    //delete m_cam;
}

void MainWindow::prepareDeviceMenu()
{
    QMenu* md = ui->menuDevice;
    md->clear();
    QList<Camera::CameraInfo> devs = Camera::enumDevices();
    foreach(const Camera::CameraInfo& info, devs)
    {
        QAction* act = md->addAction(info.name);
        act->setData(info.serial);
        act->setCheckable(true);
        if (m_cam && info.name==m_cam->cameraInfo().name && info.serial == m_cam->cameraInfo().serial) {
            act->setChecked(true);
        }
        connect(act,SIGNAL(triggered(bool)),this,SLOT(setDevice()));
    }
}

void MainWindow::setDevice()
{
    QAction* act = qobject_cast<QAction*>(sender());
    if (act)
    {
        QString name = act->text();
        QString serial = act->data().toString();
        if (name != m_cam->cameraInfo().name || serial != m_cam->cameraInfo().serial)
        {
            Camera::CameraInfo newcam;
            QList<Camera::CameraInfo> devs = Camera::enumDevices();
            foreach(const Camera::CameraInfo& info, devs)
            {
                if (info.name==name && info.serial == serial) {
                    newcam = info;
                    break;
                }
            }
            if (!newcam.serial.isEmpty())
            {
                GraphicsView* view = (GraphicsView*)centralWidget();
                view->setCamera(0);
                m_cam->stop();
                QCoreApplication::processEvents(QEventLoop::AllEvents,100);
                view->setCamera(m_cam);
                m_cam->open(newcam);
                updateCameraProperties();

            }

        }

    }
}

void MainWindow::on_actionZoomOne2One_triggered()
{
    //QWidget* w = centralWidget();
    QSize sz = Camera::frameSize();
    GraphicsView* view = (GraphicsView*)centralWidget();
    view->setMinimumSize(sz);
    adjustSize();
    view->setMinimumSize(Camera::minimumViewSize());
}

void MainWindow::on_sbGain_valueChanged(int g)
{
    m_cam->setGain(g);
}

void MainWindow::on_sldExposure_valueChanged(int p)
{
    int exp = 1000000/Camera::shutterSpeedStops[p];
    m_cam->setExposure(exp);
    ui->lblExposure->setText(QString("1/%1").arg(Camera::shutterSpeedStops[p]));
}

void MainWindow::onCameraPropertiesChanged()
{

}

void MainWindow::onCameraStatusChanged()
{
    QString msg = m_cam->statusMsg();
    GraphicsView* view = (GraphicsView*)centralWidget();
    qreal f = view->getScalingFactor();
    msg += QString(", MAG: x%1").arg(f,4,'f',2);
    m_camStatus->setText(msg);
}


void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    m_cam->setColormap((VideoFrame::Colormap)index);
}

void MainWindow::updateCameraProperties()
{
    ui->cameraName->setText(m_cam->cameraInfo().name);

    ui->sbGain->blockSignals(true);
    ui->sldGain->blockSignals(true);
    ui->sldExposure->blockSignals(true);

    ui->sbGain->setValue(m_cam->gain());
    ui->sldGain->setValue(m_cam->gain());
    int e = m_cam->exposure();
    int denom = 1000000/e;
    int p=0;
    while (denom<Camera::shutterSpeedStops[p] && p<Camera::shutterSpeedStopsN) p++;
    ui->sldExposure->setValue(p);
    ui->lblExposure->setText(QString("1/%1").arg(Camera::shutterSpeedStops[p]));

    ui->sbGain->blockSignals(false);
    ui->sldGain->blockSignals(false);
    ui->sldExposure->blockSignals(false);

}





void MainWindow::on_sbBrightness_valueChanged(int b)
{
    m_cam->setBrightness(b);
}

void MainWindow::on_sbContrast_valueChanged(int arg1)
{
    m_cam->setContrast(arg1);
}

void MainWindow::on_sbGamma_valueChanged(double arg1)
{
    m_cam->setGamma(arg1);
    ui->sldGamma->blockSignals(true);
    ui->sldGamma->setValue((int)(log10(arg1)*127));
    ui->sldGamma->blockSignals(false);
}

void MainWindow::on_sldGamma_valueChanged(int v)
{
    // -127 < v < 127
    qreal g = pow(10.,1.*v/127);
    ui->sbGamma->setValue(g);

}

void MainWindow::on_actionROI_triggered(bool checked)
{
    GraphicsView* view = (GraphicsView*)centralWidget();
    view->onRoiOn(checked);
    if (checked) {
        m_cam->setRoi(QRect());
        ui->actionZoom_to_ROI->blockSignals(true);
        ui->actionZoom_to_ROI->setChecked(false);
        ui->actionZoom_to_ROI->blockSignals(false);
    }
}

void MainWindow::on_actionZoom_to_ROI_triggered(bool checked)
{
    GraphicsView* view = (GraphicsView*)centralWidget();
    view->onZoom2Roi(checked);
    if (checked) {
        m_cam->setRoi(view->roi());
        ui->actionROI->blockSignals(true);
        ui->actionROI->setChecked(false);
        ui->actionROI->blockSignals(false);
    } else m_cam->setRoi(QRect());

}
