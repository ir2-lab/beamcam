#ifndef APPSINK_H
#define APPSINK_H

#include <QObject>
#include <QMutex>

#include <QGst/Utils/ApplicationSink>
#include <QGst/Sample>

#include "counter.h"

class AppSink : public QObject, public QGst::Utils::ApplicationSink
{
    Q_OBJECT
public:
    explicit AppSink(QObject *parent = 0);

    void init(QGst::ElementPtr ptr);

    qreal fps() const { return ctr.fps(); }
    void reset() { ctr.reset(); }


    bool getRawPixels(uchar* pixels, int sz);

signals:
    void sampleAvailable();

public slots:

protected:
    virtual QGst::FlowReturn newSample();

    QGst::SamplePtr m_sample;
    QMutex mtx;
    counter ctr;
};




#endif // APPSINK_H
