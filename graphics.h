#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <QGraphicsView>
#include <QGraphicsScene>
//#include <QPoint>

namespace QtCharts {
    class QChart;
}

using namespace QtCharts;

class PixmapItem;
class QResizeEvent;
class LineCursor;
class RoiRect;
class CursorChart;
class Histogram;
class Camera;
class VideoFrame;

class GraphicsScene : public QGraphicsScene
{
public:
    GraphicsScene();

    PixmapItem* videoItem;
    CursorChart *hChart, *vChart;
    LineCursor *hCursor, *vCursor;
    RoiRect *roiRect;
    Histogram *hist;

    bool cursorsOn() const { return m_cursorsOn; }
    void setCursorsOn(bool flag);
    bool histoOn() const { return m_histoOn; }
    void setHistoOn(bool flag);
    void setRoiOn(bool flag);
    void setZoom2Roi(bool flag);

    qreal scalingFactor(const QSize& sz) const;
    QRect roi() const;
    QPoint crossHair() const;

    void adjustLineWidths(qreal f);

    bool isZoom2Roi() const { return m_zoom2Roi; }

protected:
    bool m_cursorsOn, m_histoOn, m_roiOn, m_zoom2Roi;

    void layoutItems();
};

class GraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    GraphicsView(QWidget* parent = 0);

    void setCamera(Camera* c);

    QRect roi() const { return m_scene->roi(); }

    void onRoiOn(bool on);
    void onZoom2Roi(bool on);

    qreal getScalingFactor() const;


protected slots:

    void onNewFrame();
    void onCursorsOn(bool on);
    void onHistoOn(bool on);


protected:
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

    Camera* m_camera;

    GraphicsScene* m_scene;
};

#endif // GRAPHICS_H
