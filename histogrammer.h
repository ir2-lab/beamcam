#ifndef HISTOGRAMMER_H
#define HISTOGRAMMER_H

#include "videoframe.h"

class Histogrammer
{
    const static int N = 1 << 8;
    const static int M = VideoFrame::width*VideoFrame::height;

public:
    Histogrammer();

    void proccess(const VideoFrame& frm);

    const QVector<qreal>& getHistogram() const  {
        return H;
    }

private:

    QVector<qreal> H; // normalized F(i)/F(max)
    QVector<quint32> F,F2; // pixel frequencies

    void calcFrequencies(const QImage &img);
    static quint32 maxval(const QVector<quint32>& V);

};

#endif // HISTOGRAMMER_H
