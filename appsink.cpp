#include "appsink.h"

#include <QMutexLocker>

#include <QGst/Buffer>
#include <QGst/Memory>
#include <QGst/Structure>
#include <QGlib/Value>


AppSink::AppSink(QObject *parent) : QObject(parent), QGst::Utils::ApplicationSink()
{

}

void AppSink::init(QGst::ElementPtr ptr)
{
    QGst::Utils::ApplicationSink::setElement(ptr);
}

QGst::FlowReturn AppSink::newSample()
{
    QGst::SamplePtr current = pullSample();

    bool s = false;
    mtx.lock();
    if (m_sample.isNull())
    {
        m_sample = current;
        s = true;
    }
    mtx.unlock();

    ctr.tic();
    if (s) emit sampleAvailable();
    return QGst::FlowOk;
}

bool AppSink::getRawPixels(uchar* pixels, int sz)
{
    QMutexLocker L(&mtx);

    QGst::SamplePtr sample = m_sample;
    m_sample.clear();

    if (!sample) return false;

    QGst::BufferPtr buff =  sample->buffer();
    QGst::MapInfo info;

    if (!buff) return false;
    if (!buff->map(info,QGst::MapRead)) return false;

    // check data size
    if (! sz==info.size() )
    {
        buff->unmap(info);
        return false;
    }

    memcpy(pixels, info.data(), sz);

    buff->unmap(info);

    return true;
}
