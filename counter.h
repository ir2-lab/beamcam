#ifndef COUNTER_H
#define COUNTER_H

#include <QTime>
#include <QList>

class counter
{
    const static int N = 100;
    QList<QTime> L;
    qreal fps_;

public:
    counter() : fps_(0.)
    {}

    void tic()
    {
        L.push_back(QTime::currentTime());
        int n = L.size()-1;
        if (!n) return;
        int t = L.front().msecsTo(L.back());
        fps_ = 1000.*n/t;
        if (n>N || t>1000) L.pop_front();
    }

    void reset()
    {
        L.clear();
        fps_ = 0.;
    }

    qreal fps() const { return fps_; }

};

#endif // COUNTER_H
