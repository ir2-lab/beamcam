#ifndef VIDEOFRAME_H
#define VIDEOFRAME_H

#include <QImage>
#include <QPixmap>
#include <QByteArray>
#include <QVector>
#include <QPointF>

#include "transferfunction.h"


class QPainter;
class AppSink;
class QLabel;
class QGraphicsPixmapItem;

class VideoFrame
{
public:
    const static int bpp = 16;
    const static int width = 1280;
    const static int height = 960;
    const static int size = width*height*bpp/8;

    enum Colormap {
        cmGray = 0,
        cmJet = 1,
        cmHot = 2,
        cmCopper = 3
    };

    VideoFrame();

    void paintHere(QPainter& p, const QRect& rect) const;
    void paintHere(QLabel* w) const;
    void paintHere(QGraphicsPixmapItem* w) const;

    bool getNewFrame(AppSink& sink);

    bool isValid() const { return valid; }

    void setColormap(Colormap cm);

    Colormap colormap() const { return m_colormap; }
    int brightness() const
    { return transferFunction.brightness(); }
    int contrast() const
    { return transferFunction.contrast(); }
    qreal gamma() const
    { return transferFunction.gamma(); }

    void setBrightness(int b)
    { transferFunction.setBrightness(b); updateColormap(); }
    void setContrast(int c)
    { transferFunction.setContrast(c); updateColormap(); }
    void setGamma(qreal g)
    { transferFunction.setGamma(g); updateColormap(); }

    void cursorData(QVector<QPointF>& v, int idx, bool hrz) const;

    const QByteArray& getPixels() const { return pixels; }
    const QImage& getImage() const { return indexed8bit; }

    const QVector<int>& getTransferCurve() const
    { return transferFunction.getCurve(); }

    const QRect& roi() const { return roi_; }
    void setRoi(const QRect& r);
    void clearRoi() { setRoi(QRect()); }



private:
    QByteArray pixels;

    QImage indexed8bit;
    QPixmap pixmap;

    QRect roi_;

    bool valid;

    TransferFunction transferFunction;

    Colormap m_colormap;
    static QRgb ColormapData[4][256];

    void updateColormap();
};

#endif // VIDEOFRAME_H
