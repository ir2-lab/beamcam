#include "histogrammer.h"

Histogrammer::Histogrammer() :
    H(N), F(N), F2(N)
{

}

void Histogrammer::proccess(const VideoFrame& frm)
{
    const QVector<int>& curve = frm.getTransferCurve();

    calcFrequencies(frm.getImage());

    if (curve.isEmpty())
    {
        quint32 fmax = maxval(F);
        for(int i=0; i<N; i++) H[i] = 1.*F[i]/fmax;
    }
    else
    {
        for(int i=0; i<N; i++) F2[i] = 0;

        for(int i=0; i<N; i++)
        {
            int j = curve[i];
            F2[j] += F[i];
        }

        quint32 fmax = maxval(F2);

        for(int i=0; i<N; i++) H[i] = 1.*F2[i]/fmax;
    }
}

void Histogrammer::calcFrequencies(const QImage &img)
{
    quint32 *fdata  = F.data();
    memset(fdata,0,sizeof(quint32)*N);

    for(int k=0; k<img.height(); k++)
    {
        const uchar* p = img.constScanLine(k);
        const uchar *pend = p + img.width();
        while(p!=pend) fdata[*p++]++;
    }
}

quint32 Histogrammer::maxval(const QVector<quint32>& V)
{
    quint32 fmax = 0;
    const quint32 *f  = V.constData();
    const quint32 *fend = f + N;
    f++; fend--;
    while(f!=fend) {
        quint32 v = *f++;
        if (v>fmax) fmax = v;
    }
    return fmax;
}


